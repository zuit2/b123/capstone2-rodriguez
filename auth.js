const jwt = require("jsonwebtoken");
const secret = process.env.JWT_SECRET;

module.exports.createAccessToken = (user) => {
    console.log(user.email);

    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin,
    };

    return jwt.sign(data, secret, {});
};

module.exports.verify = (req, res, next) => {
    let token = req.headers.authorization;
    // console.log(token);

    if (typeof token === "undefined") {
        return res.send({ auth: "Failed. No Token" });
    } else {
        token = token.slice(7, token.length);
        // console.log(token);

        jwt.verify(token, secret, (err, decodedToken) => {
            if (err) {
                return res.send({
                    auth: "Failed",
                    message: err.message,
                });
            } else {
                console.log(decodedToken);
                console.log("-verified user");
                req.user = decodedToken;
                next();
            }
        });
    }
};

module.exports.verifyAdmin = (req, res, next) => {
    if (req.user.isAdmin) {
        console.log("-verified admin");
        next();
    } else {
        return res.send({
            auth: "Failed",
            message: "Action Forbidden",
        });
    }
};
