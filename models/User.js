const mongoose = require('mongoose');
const userSchema = new mongoose.Schema({

	email: {
		type: String,
		required: [true, "Email is required."]
	},
	password: {
		type: String,
		required: [true, "Password is required."]
	},
	firstName: {
		type: String,
		required: [true, "First name is required."]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required."]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile no. is required."]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	dateRegistered: {
		type: Date,
		default: new Date()
	},
	orders: [
		{
			orderStatus: { //for future enhancement, shopping carts (Incomplete/Complete/Ordered etc.)
				type: String,
				default: "Complete"
			},
			dateCreated: {
				type: Date,
				default: new Date()
			},
			userId: {
				type: String,
				required: [true, "User Id is required."]
			},
			totalAmount: {
				type: Number,
				default: 0
			},
			products:[
				{

					itemStatus: { //for enchancements, shopping carts (Created/Deleted)
						type: String,
						default: "Created"
					},
					productId: {
						type: String,
						required: [true, "Product Id is required."]
					},
					productName: {
						type: String,
						required: [true, "Product name is required."]
					},
					quantity: {
						type: Number,
						required: [true, "Product quantity is required."]
					},
					priceSold: {
						type: Number,
						required: [true, "Price is required."]
					}/*,
					subTotalAmount: {
						type: Number,
						required: [true, "Subtotal amount is required."]
					},
					dateAdded: {
						type: Date,
						default: new Date()
					}*/
				}
			]
		}
	]
})

module.exports = mongoose.model("User",userSchema);