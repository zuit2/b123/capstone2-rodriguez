const mongoose = require('mongoose');
const productSchema = new mongoose.Schema({

	productName: {
		type: String,
		required: [true, "Product name is required."]
	},
	description: {
		type: String,
		required: [true, "Product description is required."]
	},
	price: { //current price
		type: Number,
		required: [true, "Product price is required."]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	dateCreated: {
		type: Date,
		default: new Date()
	},
	orders: [
		{
			userId: {
				type: String,
				required: [true, "User ID is required."]
			},
			orderStatus: {
				type: String,
				default: "Complete"
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required."]
			},
			priceSold: { //price when ordered
				type: Number,
				required: [true, "Price sold is required."]
			},/*
			subTotalAmount: {
				type: Number,
				required: [true, "Sub Total Amount is required."]
			},*/
			datePurchased: {
				type: Date,
				default: new Date()
			}
		}
	]
});

module.exports = mongoose.model("Product",productSchema);