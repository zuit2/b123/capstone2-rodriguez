const Product = require("../models/Product");

module.exports.getAllProducts = (req, res) => {
    Product.find({}, { orders: 0 })
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};

module.exports.getActiveProducts = (req, res) => {
    Product.find({ isActive: true }, { orders: 0 })
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};

module.exports.getSingleProduct = (req, res) => {
    console.log(req.params.id);

    Product.findOne({ _id: req.params.id }, { orders: 0 })
        .then((foundProduct) => {
            console.log(foundProduct.productName);
            if (foundProduct === null) {
                return res.send({
                    message: `Product not found. ID: ${req.params.id}`,
                });
            } else {
                return res.send(foundProduct);
            }
        })
        .catch((err) => res.send(err));
};

module.exports.createProduct = (req, res) => {
    // console.log(req.body);

    let newProduct = new Product({
        productName: req.body.productName,
        description: req.body.description,
        price: req.body.price,
    });
    newProduct
        .save()
        .then((result) => {
            console.log("-product created");
            console.log(result);
            return res.send({
                status: "success",
                message: `New product (${result.productName}) has been created`,
            });
        })
        .catch((err) => res.send(err));
};

module.exports.updateProduct = (req, res) => {
    console.log(req.params);

    let updates = {
        productName: req.body.productName,
        description: req.body.description,
        price: req.body.price,
        isActive: req.body.isActive,
    };

    Product.findByIdAndUpdate(req.params.id, updates, { new: true })
        .then((updatedProduct) => {
            console.log("-product updated");
            console.log(updatedProduct);
            return res.send({
                message: `Product (${updatedProduct.productName}) has been updated.`,
            });
        })
        .catch((err) => res.send(err));
};

module.exports.archiveProduct = (req, res) => {
    console.log(req.params);

    let updates = {
        isActive: false,
    };

    Product.findByIdAndUpdate(req.params.id, updates, { new: true })
        .then((archivedProduct) => {
            console.log("-product archived");
            // console.log(archivedProduct);
            return res.send({
                status: "success",
                message: `Product (${archivedProduct.productName}) has been disabled.`,
            });
        })
        .catch((err) => res.send(err));
};

module.exports.activateProduct = (req, res) => {
    console.log(req.params);

    let updates = {
        isActive: true,
    };

    Product.findByIdAndUpdate(req.params.id, updates, { new: true })
        .then((activatedProduct) => {
            console.log("-product activated");
            // console.log(activatedProduct);
            return res.send({
                status: "success",
                message: `Product (${activatedProduct.productName}) has been enabled.`,
            });
        })
        .catch((err) => res.send(err));
};
