const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const { createAccessToken } = auth;

module.exports.verifyEmail = (req, res, next) => {
    console.log(req.body);

    User.findOne({ email: req.body.email })
        .then((result) => {
            if (result === null) {
                next();
            } else {
                return res.send({
                    isAvailable: false,
                    message: "Email is already registered.",
                });
            }
        })
        .catch((err) => res.send(err));
};

module.exports.registerUser = (req, res) => {
    console.log(req.body);

    if (req.body.password.length < 8)
        return res.send({
            message:
                "Password is too short. At least 8 characters is required.",
        });

    const hashedPW = bcrypt.hashSync(req.body.password, 10);
    console.log(hashedPW);

    let newUser = new User({
        email: req.body.email,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        mobileNo: req.body.mobileNo,
        password: hashedPW,
    });
    newUser
        .save()
        .then((result) => {
            console.log("-user registered");
            console.log(result);
            return res.send({
                status: true,
                message: `New user profile has been created for (${result.email})`,
            });
        })
        .catch((err) => res.send(err));
};

module.exports.loginUser = (req, res) => {
    console.log("-log in controller");
    console.log(req.body);

    User.findOne({ email: req.body.email })
        .then((foundUser) => {
            if (foundUser === null) {
                return res.send({
                    message: "Username or password is invalid.",
                });
            } else {
                console.log("-user found");
                console.log(req.body.email);
                // console.log(req.body.password);

                const isPasswordCorrect = bcrypt.compareSync(
                    req.body.password,
                    foundUser.password
                );
                console.log(isPasswordCorrect);

                if (isPasswordCorrect) {
                    console.log("-logged in");
                    return res.send({
                        message: `Welcome, ${foundUser.firstName}!`,
                        accessToken: createAccessToken(foundUser),
                    });
                } else {
                    return res.send({
                        message: `Username or password is invalid.`,
                    });
                }
            }
        })
        .catch((err) => res.send(err));
};

module.exports.getSingleUser = (req, res) => {
    console.log(req.user);

    User.findById(req.user.id)
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};

module.exports.setAdmin = (req, res) => {
    console.log(req.params.id);

    let updates = {
        isAdmin: true,
    };

    User.findByIdAndUpdate(req.params.id, updates, { new: true })
        .then((updatedUser) => {
            console.log("-user updated");
            console.log(updatedUser);
            return res.send({
                status: true,
                message: `User profile has been set to Admin for (${updatedUser.email}).`,
            });
        })
        .catch((err) => res.send(err));
};

module.exports.createOrder = async (req, res) => {
    // console.log(req.body);

    if (req.user.isAdmin) {
        return res.send({
            auth: "Failed",
            message: "Action Forbidden",
        });
    }

    let productsArray = [];

    // console.log(req.body.products);

    productsArray = req.body.products.map((product) => {
        console.log(product);
        return {
            productId: product.id,
            productName: product.productName,
            priceSold: product.price,
            quantity: product.quantity,
        };
    });

    // console.log(productsArray);

    let isUserUpdated = await User.findById(req.user.id).then((user) => {
        user.orders.push({
            products: productsArray,
            totalAmount: req.body.totalAmount,
            userId: req.user.id,
        });

        return user
            .save()
            .then((user) => true)
            .catch((err) => err.message);
    });

    console.log(isUserUpdated);

    if (isUserUpdated !== true) return res.send(isUserUpdated);

    let isProductUpdate = false;

    for (index = 0; index < productsArray.length; index++) {
        // console.log(index);

        isProductUpdated = await Product.findById(
            productsArray[index].productId
        )
            .then((product) => {
                if (product !== null) {
                    product.orders.push({
                        userId: req.user.id,
                        quantity: productsArray[index].quantity,
                        priceSold: productsArray[index].priceSold,
                    });
                    return product
                        .save()
                        .then((product) => true)
                        .catch((err) => err.message);
                }
            })
            .catch((err) => err.message);
    }

    console.log(isProductUpdated);

    if (isProductUpdated !== true) return res.send(isProductUpdated);

    if (isUserUpdated && isProductUpdated)
        return res.send({
            status: "success",
            message: "Your order has been checked out successfully.",
        });

    /*let foundProduct = await Product.findOne({_id:req.body.productId})
	.then(result => result)
	.catch(err=>res.send(err));

	console.log('----foundProduct');
	console.log(foundProduct);
	console.log(req.user.id);

	let isUserUpdated = await User.findById(req.user.id)
	.then(currentUser => {
		console.log(currentUser);
		let productSubToal =  req.body.quantity * foundProduct.price;
		console.log(`productSubTotal:${productSubTotal}`);
		currentUser.orders.findOne({orderStatus:"Incomplete"})
		.then(incompleteOrder =>{

			console.log(incompleteOrder);

			if(incompleteOrder !== null) {

					incompleteOrder.products.push({
						productId: foundProduct._id,
						productName: foundProduct.productName,
						quantity: req.body.quantity,
						priceSold: foundProduct.price,
						subTotalAmount: productSubTotal
				})
				return incompleteOrder.save()
				.then(user => {
					console.log('new product added to existing order');
					return true;
				})
				.catch(err => err.message);
			} else { 

				console.log(incompleteOrder);
				currentUser.orders.push({userId: req.user.id, totalAmount: productSubTotal})
				.then(newOrder => {
					console.log(newOrder);
					let currentOrder = newOrder;
					newOrder.products.push({

						productId: foundProduct._id,
						productName: foundProduct.productName,
						quantity: req.body.quantity,
						priceSold: foundProduct.price,
						subTotalAmount: productSubTotal
					})
					newOrder.save()
					.then(user => {
						console.log('new product added to new order');
						return true;
					})
					.catch(err => err.message);
				})
				.catch(err => err.message);

			}


		})
		.catch(err => res.send(err));
		
	})
	.catch(err => res.send(err));

	console.log(isUserUpdated);

	if (isUserUpdated !== true) return res.send(isUserUpdated);

	let isProductUpdated = await Product.findById({_id: req.body.productId}).then(product =>{

			product.orders.push({
				orderId: currentOrder._id,
				orderStatus: currentOrder.orderStatus,
				quantity: req.body.quantity,
				priceSold: foundProduct.price,
				subTotalAmount: productSubTotal,
			})

			product.save()
			.then(product => true)
			.catch(err => res.send(err));
	})
	.catch(err => res.send(err));

	console.log(isProductUpdated);

	if(isProductUpdated !== true) return res.send(isProductUpdated);

	if(isUserUpdated && isProductUpdated) return res.send("Product added to order successfully.")*/
};

module.exports.getAllOrders = (req, res) => {
    let arrOrders = [];
    User.find()
        .then((users) => {
            users.forEach((user) => {
                user.orders.forEach((order) => {
                    if (order !== null) {
                        arrOrders.push(order);
                    }
                });
            });
            /*const response = {
			success: true,
			result: arrOrders
		}*/
            res.send(arrOrders);
        })
        .catch((err) => res.send(err));
};

module.exports.getMyOrders = (req, res) => {
    if (req.user.isAdmin) {
        return res.send({
            auth: "Failed",
            message: "Action Forbidden",
        });
    }
    User.findById(req.user.id)
        .then((foundUser) => {
            // console.log(foundUser)
            return res.send(foundUser.orders);
        })
        .catch((err) => res.send(err));
};

module.exports.getOrder = (req, res) => {
    const foundUser = User.findOne({ "orders._id": req.params.id })
        .then((foundUser) => {
            if (
                foundUser == null ||
                (!req.user.isAdmin && req.user.id !== foundUser.id)
            ) {
                return res.send({
                    status: "notfound",
                    message: `Order not found. #${req.params.id}`,
                });
            } else {
                const foundOrder = foundUser.orders.find((order) => {
                    return order._id.toString() === req.params.id;
                });
                // console.log(`foundOrder: ${foundOrder}`);
                return res.send(foundOrder);
            }
        })
        .catch((error) => {
            res.send(error);
        });
};
