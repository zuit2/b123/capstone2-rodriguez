const express = require('express');
const router = express.Router();
const productControllers = require('../controllers/productControllers');
const auth = require('../auth');

const {
	verify,
	verifyAdmin
} = auth;

/*
-Retrieve all active products
-Retrieve single product
-Create Product (Admin only)
-Update Product Information (Admin only)
-Archive Product (Admin only)
-Activate Product (Admin only)
-Display Products per Order (nonAdmin)
*/
const {
	getAllProducts,
	getActiveProducts,
	getSingleProduct,
	createProduct,
	updateProduct,
	archiveProduct,
	activateProduct
} = productControllers;

router.get('/all',verify,verifyAdmin,getAllProducts);
router.get('/', getActiveProducts);
router.get('/:id', getSingleProduct);
router.post('/', verify,verifyAdmin,createProduct);
router.put('/:id', verify,verifyAdmin,updateProduct);
router.put('/archive/:id', verify,verifyAdmin,archiveProduct);
router.put('/activate/:id', verify,verifyAdmin,activateProduct);

module.exports = router;