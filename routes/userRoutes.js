const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers");
const auth = require("../auth");

const { verify, verifyAdmin } = auth;

/*
-User registration
-User authentication
-Set user as admin (Admin only)
-Non-admin User checkout (Create Order)
-Retrieve authenticated user’s orders
-Retrieve all orders (Admin only)

*/

const {
    verifyEmail,
    registerUser,
    loginUser,
    getSingleUser,
    setAdmin,
    createOrder,
    getAllOrders,
    getMyOrders,
    getOrder,
} = userControllers;

router.post("/", verifyEmail, registerUser);
router.post("/login", loginUser);
router.get("/getUserDetails", verify, getSingleUser);
router.put("/setAdmin/:id", verify, verifyAdmin, setAdmin);
router.post("/createOrder", verify, createOrder);
router.get("/allOrders", verify, verifyAdmin, getAllOrders);
router.get("/myOrders", verify, getMyOrders);
router.get("/order/:id", verify, getOrder); //fix for admin

module.exports = router;
